package mbd.project.writable;

import mbd.project.mapper.AisLatLonReadMapper;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 */
public class AisCounterResultWritable implements Writable {

    String month;
    int inbound_count;
    int outbound_count;
    int message_count;

    public AisCounterResultWritable() {}

    public AisCounterResultWritable(String month, int inbound_count, int outbound_count, int message_count)
    {
        this.month = month;
        this.inbound_count = inbound_count;
        this.outbound_count = outbound_count;
        this.message_count = message_count;
    }


    @Override
    public void write(DataOutput out) throws IOException {
        out.writeChars(month);
        out.writeInt(inbound_count);
        out.writeInt(outbound_count);
        out.writeInt(message_count);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.month = WritableUtils.readString(in);
        this.inbound_count = in.readInt();
        this.outbound_count = in.readInt();
        this.message_count = in.readInt();
    }

    @Override
    public String toString() {
        return this.month + "\t" + String.valueOf(inbound_count) + "\t" + String.valueOf(outbound_count) + "\t" + String.valueOf(message_count);
    }

}
