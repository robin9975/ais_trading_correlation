
package mbd.project;

import mbd.project.mapper.AisLatLonReadMapper;
import mbd.project.partitioner.IdMonthPartitioner;
import mbd.project.reducer.ActivityCounterReducer;
import mbd.project.reducer.WriteValuesReducer;
import mbd.project.writable.AisCounterResultWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 * Class for use in MapReduce, counts the amount of ais messages per vessel
 */
public class AisCounter {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length < 2) {
            System.err.println("Usage: AisCounter <in> [<in>...] <out>");
            System.exit(2);
        }

        Job job = new Job(conf, "AIS message counter");

        job.setNumReduceTasks(80);

        job.setJarByClass(mbd.project.AisCounter.class);
        job.setMapperClass(AisLatLonReadMapper.class);
        job.setReducerClass(ActivityCounterReducer.class);
        job.setPartitionerClass(IdMonthPartitioner.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(AisCounterResultWritable.class);

        job.setMapOutputValueClass(MapWritable.class);

        for (int i = 0; i < otherArgs.length - 1; ++i) {
            FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
        }
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[otherArgs.length - 1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}