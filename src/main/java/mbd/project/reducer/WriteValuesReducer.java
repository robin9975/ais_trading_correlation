package mbd.project.reducer;

import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Simple Reducer for use in MapReduce,
 * just writes the values it gets to the output file
 */
public class WriteValuesReducer extends Reducer<Text, MapWritable, Text, Text> {

    public void reduce(Text key, Iterable<MapWritable> measurements, Context context) throws IOException, InterruptedException {

        for (MapWritable measurement : measurements) {
            String output_text = "";

            Iterable<Writable> keys = measurement.keySet();
            for (Writable map_key : keys) {
                output_text += String.valueOf(map_key)+": "+String.valueOf(measurement.get(map_key)) + ", ";
            }

            context.write(key, new Text(output_text));
        }

    }
}
