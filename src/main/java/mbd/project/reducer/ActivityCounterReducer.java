package mbd.project.reducer;

import mbd.project.writable.AisCounterResultWritable;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.*;

/**
 * Simple Reducer for use in MapReduce,
 */
public class ActivityCounterReducer extends Reducer<Text, MapWritable, Text, AisCounterResultWritable> {

    public void reduce(Text key, Iterable<MapWritable> measurements, Context context) throws IOException, InterruptedException {

        // sort measurements on timestamp, by adding them to a new map
        TreeMap<String, Boolean> sorted_tree = new TreeMap<>();
        String month = "";

        for (MapWritable measurement : measurements) {
            String timestamp = String.valueOf(measurement.get(new Text("timestamp")));
            month = String.valueOf(measurement.get(new Text("month")));
            BooleanWritable xx = (BooleanWritable) measurement.get(new Text("inNL"));
            Boolean inNetherlands = (xx.get());

            sorted_tree.put(timestamp, inNetherlands);
        }

        boolean current_inNL = sorted_tree.firstEntry().getValue();;
        int outbound_count = 0;
        int inbound_count = 0;
        for (Map.Entry<String, Boolean> entry : sorted_tree.entrySet()) {
            boolean new_inNl = entry.getValue();
            if (current_inNL == new_inNl) continue;

            if (current_inNL) outbound_count++; // if now in NL and next one is out of NL
            if (!current_inNL) inbound_count++; // if now NOT in NL and next one is in NL

            current_inNL = new_inNl;
        }

        if (inbound_count == 0 && outbound_count == 0) return;
        AisCounterResultWritable outputWritable = new AisCounterResultWritable(month, inbound_count, outbound_count, sorted_tree.size());

        context.write(key, outputWritable);
    }
}
