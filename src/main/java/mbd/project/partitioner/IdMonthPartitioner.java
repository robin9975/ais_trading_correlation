package mbd.project.partitioner;

import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class IdMonthPartitioner extends Partitioner<Text, MapWritable> {

    @Override
    public int getPartition(Text key, MapWritable value, int numPartitions) {
        int int_key = Integer.parseInt(key.toString());
        int key_partition = int_key % 10;

        int month_partition = 0;
        try {
            month_partition = getPartitionFromMonth(value.get(new Text("month")).toString());
        } catch (Exception e) {
            System.out.print("Something went wrong in the partitioner..");
            return 0;
        }

        return key_partition * 8 + month_partition;
    }


    private int getPartitionFromMonth(String month) throws Exception
    {
//        if (month.equals("<2014-06")) return 0;
//        if (month.equals("2014-06")) return 1;
//        if (month.equals("2014-07")) return 2;
//        if (month.equals("2014-08")) return 3;
//        if (month.equals("2014-09")) return 4;
        if (month.equals("<2014-10")) return 0;
        if (month.equals("2014-11")) return 1;
        if (month.equals("2014-12")) return 2;
        if (month.equals("2015-01")) return 3;
        if (month.equals("2015-02")) return 4;
        if (month.equals("2015-03")) return 5;
        if (month.equals("2015-04")) return 6;
        if (month.equals(">2015-05")) return 7;
//        if (month.equals("2015-06")) return 13;
//        if (month.equals(">2015-07")) return 14;

        throw new Exception("We should not be able to hit this code...");
    }
}
