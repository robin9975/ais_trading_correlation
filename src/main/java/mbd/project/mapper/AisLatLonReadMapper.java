package mbd.project.mapper;

import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.Polygon;
import java.io.IOException;
import java.util.Map;

/**
 * Mapper to read the AIS Json messages and output the Latitude and Longitude as text
 */
public class AisLatLonReadMapper extends Mapper<Object, Text, Text, MapWritable> {

    private Polygon bounds;

    public AisLatLonReadMapper() {
        int[] xs = new int[] {33645, 30789, 39166, 41500, 42407, 44549, 51965, 61468, 68444, 69598, 71850, 70422, 67291, 67071, 70587, 70642, 66906, 68499, 63830, 61358, 59381, 62292, 60919, 60150, 56799, 58062, 52185, 47680, 44165, 42242, 41198, 38287, 34606};
        int[] ys = new int[] {513666, 515907, 522614, 525467, 529122, 532947, 535696, 536446, 536250, 533866, 532454, 526497, 526463, 524794, 524258, 522412, 520660, 519814, 518357, 519103, 518527, 514334, 511380, 507607, 507677, 511621, 512825, 514984, 513649, 513683, 512722, 512000, 512516};

        this.bounds = new Polygon(xs, ys, 33);
    }

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        Text idString = new Text();

        MapWritable ais_measurement = new MapWritable();
        JSONParser parser = new JSONParser();
        Map aisMessage;

        try {
            aisMessage = (Map<String, Object>) parser.parse(value.toString());
        } catch (ClassCastException | ParseException e) {
            return; // do nothing (we might log this)
        }

        String lat = String.valueOf(aisMessage.get("lat"));
        String lon = String.valueOf(aisMessage.get("lon"));
        int in_nl = this.inNL(lat, lon);
        if (in_nl == -1) return;

        BooleanWritable inNLRes = new BooleanWritable(in_nl == 1);

        idString.set(String.valueOf(aisMessage.get("mmsi")));
        ais_measurement.put(new Text("timestamp"), new Text(String.valueOf(aisMessage.get("ts"))));
        ais_measurement.put(new Text("inNL"), inNLRes);
        ais_measurement.put(new Text("month"), new Text(getMonthFromTimestamp(Integer.parseInt(String.valueOf(aisMessage.get("ts"))))));

        context.write(idString, ais_measurement);
    }

    private int inNL(String lat, String lon) {
        // Rough bounding box: 3.12, 50.56, 7.76, 53.81
        Double latD, lonD;
        try {
            latD = Double.valueOf(lat);
            lonD = Double.valueOf(lon);
        } catch (NumberFormatException e) {
            return -1;
        }

        // Do a rough check first
        boolean inBox = latD > 50.56 && latD < 53.81 && lonD > 3.12 && lonD < 7.76;

        // Check in greater detail
        // 12 'zeemijl' equals 22.22 kilometers
        if (inBox) {
            inBox = this.bounds.contains(lonD * 10000.0, latD * 10000.0);
        }

        return inBox ? 1 : 0;
    }

    private String getMonthFromTimestamp(int timestamp)
    {
        if (timestamp < 1401573600) return "<2014-06";

        if (timestamp < 1404165600) return "2014-06";
        if (timestamp < 1406844000) return "2014-07";
        if (timestamp < 1409522400) return "2014-08";
        if (timestamp < 1412114400) return "2014-09";
        if (timestamp < 1414796400) return "2014-10";
        if (timestamp < 1417388400) return "2014-11";
        if (timestamp < 1420066800) return "2014-12";

        if (timestamp < 1422745200) return "2015-01";
        if (timestamp < 1425164400) return "2015-02";
        if (timestamp < 1427839200) return "2015-03";
        if (timestamp < 1430431200) return "2015-04";
        if (timestamp < 1433109600) return "2015-05";
        if (timestamp < 1435701600) return "2015-06";

        return ">2015-06";
    }

}
