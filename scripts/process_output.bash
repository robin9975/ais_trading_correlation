#!/usr/bin/env bash
echo "month,inbound,outbound,total_messages"
for month in "2014-08" "2014-09" "2014-10" "2014-11" "2014-12" "2015-01" "2015-02" "2015-03" "2015-04" "2015-05"
do
    hadoop fs -cat $1 | grep $month | awk '{inbound+=$3;outbound+=$4;month=$2;total+=$5} END {print month,inbound,outbound,total}'
done