# Managing big data

This repository holds the code for the group assignment of group 9 for Managing Big Data in January 2015 at the University Of Twente. 

## Introduction

It is possible to investigate the relation between AIS data and international trade numbers. From the literature, a direct relation between AIS data and international trade numbers (and the research in that field) is missing. Big data tools that are nowadays available enable the processing of this data and may give more insight in these numbers, which is the aim of the research this code is for.

This code holds a MapReduce program and bash script to count the inbound and outbound ships to and from the Netherlands, 
based on AIS data. AIS data is provided in JSON format on the cluster CTIT at the University of Twente.

## Running the code

This code should be run on a hadoop cluster. Once all the files are put, the code can be built using maven

    mvn package
    
Then, it is possible to run the code using

    hadoop jar target/bigdata-0.2.jar mbd.project.AisCounter <input_file(s)> <output_dir>
    
Where the input_file(s) and the output_dir are locations on the hadoop cluster. This will output the data from the MapReduce program, 
which is all the inbound/outbound counts per month, per vessel id. To get the total counts per month, 
you should run the bash script provided in the scripts directory

    ./scripts/process_output.bash out_with_total_counts/*
    
## Authors

R.W.P. Hoogervorst
S.W.R. Niesink
S.A. Stevens

{r.w.p.hoogervorst,s.w.r.niesink-1,s.a.stevens}@student.utwente.nl